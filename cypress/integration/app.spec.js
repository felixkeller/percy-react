// type definitions for Cypress object "cy"
/// <reference types="cypress" />

describe("App", function () {
  beforeEach(function () {
    cy.visit("/");
    // Take a snapshot for visual diffing
    cy.percySnapshot();
  });

  it("renders the app", function () {
    cy.get(".App-link").should("contain", "Learn React");
  });
});
